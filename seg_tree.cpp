#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::vector;

template <typename T>
class SegmentTree {
 private:
  struct Node {
    T max_prefix_sum;
    T sum;
    T max_suffix_sum;
    T max_sum;
    Node() = default;
    Node(T item)
        : max_prefix_sum(item),
          max_suffix_sum(item),
          max_sum(item),
          sum(item) {}

    Node(T max_prefix_sum, T sum, T max_suffix_sum, T max_sum)
        : max_prefix_sum(max_prefix_sum),
          sum(sum),
          max_suffix_sum(max_suffix_sum),
          max_sum(max_sum) {}
  };
  int size;
  Node max(const Node &firstNode, const Node &secondNode);
  vector<Node> tree;
  void build(const vector<T> &array, int vertex, int curLeft, int curRight);
  Node query(int vertex, int left, int right, int curLeft, int curRight);

 public:
  SegmentTree(const vector<T> &array);
  T getMaxSum(size_t left, size_t right);
};

int main() {
  int arraySize;
  while (cin >> arraySize) {
    int queriesCount;
    cin >> queriesCount;
    vector<int> array(arraySize);
    for (auto &it : array) cin >> it;
    SegmentTree<int> tree(array);
    for (int i = 0; i < queriesCount; i++) {
      int left, right;
      cin >> left >> right;
      left--;
      right--;
      cout << tree.getMaxSum(left, right) << std::endl;
    }
  }
}

template <typename T>
SegmentTree<T>::SegmentTree(const vector<T> &array) {
  tree.resize(4 * array.size());
  size = array.size() - 1;
  build(array, 1, 0, array.size() - 1);
}

template <typename T>
void SegmentTree<T>::build(const vector<T> &array, int vertex, int curLeft,
                           int curRight) {
  if (curLeft == curRight) {
    tree[vertex] = Node(array[curLeft]);
    return;
  }
  int middle = (curLeft + curRight) / 2;
  build(array, vertex * 2, curLeft, middle);
  build(array, vertex * 2 + 1, middle + 1, curRight);
  tree[vertex] = max(tree[vertex * 2], tree[vertex * 2 + 1]);
}

template <typename T>
typename SegmentTree<T>::Node SegmentTree<T>::max(
    const typename SegmentTree<T>::Node &firstNode,
    const typename SegmentTree<T>::Node &secondNode) {
  return {std::max(firstNode.max_prefix_sum,
                   firstNode.sum + secondNode.max_prefix_sum),
          firstNode.sum + secondNode.sum,
          std::max(secondNode.max_suffix_sum,
                   secondNode.sum + firstNode.max_suffix_sum),
          std::max(firstNode.max_sum, std::max(secondNode.max_sum,
                                               firstNode.max_suffix_sum +
                                                   secondNode.max_prefix_sum))};
}

template <typename T>
T SegmentTree<T>::getMaxSum(size_t left, size_t right) {
  return query(1, left, right, 0, size).max_sum;
}

template <typename T>
typename SegmentTree<T>::Node SegmentTree<T>::query(int vertex, int left,
                                                    int right, int curLeft,
                                                    int curRight) {
  if (left == curLeft && right == curRight) return tree[vertex];
  int middle = (curLeft + curRight) / 2;
  if (right <= middle)  // Полностью в левой
    return query(vertex * 2, left, right, curLeft, middle);
  if (left > middle)
    return query(vertex * 2 + 1, left, right, middle + 1, curRight);
  return max(query(vertex * 2, left, middle, curLeft, middle),
             query(vertex * 2 + 1, middle + 1, right, middle + 1, curRight));
}
